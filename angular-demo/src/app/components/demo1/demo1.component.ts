import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.less']
})
export class Demo1Component implements OnInit {

  public title: string = "title";
  public desc: string = "我是title属性"
  //public list: any[]
  public list: Array<any> = ["吃饭", "睡觉", "打豆豆"];
  public json: Array<object> = [{
    "name": "张三",
    "age": 18,
    "sex": "男"
  }, {
    "name": "李四",
    "age": 19,
    "sex": "女"
  }, {
    "name": "王五",
    "age": 20,
    "sex": "男"
  }]

  public flag: boolean = true;

  public bdLogo: string = "https://www.baidu.com/img/bd_logo1.png?where=super";

  public money: number = 1000

  public today: object = new Date()

  public keywords: string = "";

  constructor() { }

  ngOnInit() {
  }


  hundleClick() {
    alert("hello world")
  }

  setData() {
    this.title = "我改变了title"
  }



}
