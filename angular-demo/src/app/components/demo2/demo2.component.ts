import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.less']
})
export class Demo2Component implements OnInit {

  peopleInfo: object = {
    "username": "",
    "sex": "1",
    "cityList": ["北京", "上海", "深圳", "广州"],
    "selectCity": "北京",
    "hobbies": [
      { "title": "吃饭", "checked": false },
      { "title": "睡觉", "checked": false },
      { "title": "敲代码", "checked": false },
      { "title": "打豆豆", "checked": false }
    ]
  };




  constructor() { }

  ngOnInit() {
  }

  doSubmit() {

  }

}
