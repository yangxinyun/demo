import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.less']
})
export class Demo3Component implements OnInit {

  constructor() { }

  public inputValue: string = ""
  public TodoList: Array<string> = localStorage.TodoList.split(",") || []


  addList() {

    this.TodoList.push(this.inputValue)
    this.inputValue = ""
    localStorage.TodoList = this.TodoList

  }
  deleteByIndex(index) {
    this.TodoList.splice(index, 1);
    localStorage.TodoList = this.TodoList
  }

  keydownhundle(e) {
    if (e.keyCode == 13) {
      this.addList()
    }
  }



  ngOnInit() {
  }

}
