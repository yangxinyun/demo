package main

import "fmt"

func main(){
	/*
	进制：
		无法直接表示二进制
		默认就是10进制
		以0开头的表示八进制

	fmt.Println(),默认打印输出10进制
	 */
	 num1 := 123 //定义一个变量，赋值为123,10进制
	 fmt.Println(num1)
	 num2 := 0132 //八进制数
	 fmt.Println(num2) //90

	 num3:=0xA2C //十六进制
	fmt.Println(num3) //2604


}
