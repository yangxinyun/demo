/*
我的第一个go程序

fmt包：关于输入输出：
	读取键盘收入，打印内容
 */
package main //包的声明
import "fmt"

//import "fmt" //导入其他的包：

func main(){// 程序的入口
	/*
	fmt包下的函数：
	Println(),打印输出括号中的内容，并换行
		print+line
	Print(),打印输出，不换行
	Printf("",),按照固定的模板去打印输出

	\n,换行
	 */
fmt.Print("Hello 王二狗。。。","\n")
fmt.Print("Hello 王二狗。。。","\n")
fmt.Print("Hello 王二狗。。。","\n")
fmt.Println("hello")


}
