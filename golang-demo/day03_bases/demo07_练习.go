package main

import "fmt"

func main()  {
	const (
		a = iota // 0
		b  //1
		c //2
		d = "ha" // iota 3
		e //iota 4
		f = 100 // iota 5
		g // iota 6
		h = iota //iota 7
		i
	)
	const(
		j = iota //0
	)
	fmt.Println(a,b,c,d,e,f,g,h,i,j)

	const (
		x = 'A' // 65
		y // 65
		m = iota //2
		n // 3
	)

	fmt.Printf("%T,%d\n",x,x) //int32 , 65
	fmt.Println(y,m,n)
	/*
	数据类型：
		整数，浮点，字符串

	二进制：计算机只识别二进制。
		自负
	 */
}
