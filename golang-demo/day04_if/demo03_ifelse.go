package main

import "fmt"

func main(){
	/*
	ifelse语句
	if 条件表达式{
		条件成立，执行此处的代码
	}else{
		条件不成立，
	}

	 */
	 age := 30
	 if age >= 22{
	 	fmt.Println("够大了，可以娶媳妇了。。")
	 }else{
	 	fmt.Println(age,"还小呢，长大再说。。")
	 }
	 fmt.Println("main...over...")

	 sex := "男性" // bool,int,'男'，"男性"
	 if sex == "泰国"{
	 	fmt.Println("去男厕所。。")
	 }else{
	 	fmt.Println("去女厕所。。")
	 }


}
