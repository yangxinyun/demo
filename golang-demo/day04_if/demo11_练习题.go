package main

import "fmt"

func main()  {

	/*
	1.使用for循环，计算5的阶乘：5!=5*4*3*2*1

2.使用for循环，打印58-23数字

3.使用for循环，打印'A'到'Z'字母

4.使用for循环，打印1-100内的数字，每行打印10个。

5.打印1-100内，能被3整除，但是不能被5整除的数，并统计个数，每行打印5个。
	 */

	 for i:=58;i>=23;i--{
	 	fmt.Println(i)
	 }
	 fmt.Println("-------------------")
	 for i:='A';i<='Z';i++{
	 	fmt.Printf("%c\t",i)
	 }
	 fmt.Println()
	 fmt.Println("-----------------------------")
	 for i:=1;i<=100;i++{//1-10,11-20,21-30
	 	fmt.Print(i,"\t")
	 	if i % 10 == 0{
	 		fmt.Println()
		}
	 }
fmt.Println("--------------------------------")
//计数器：数数
	count := 0//统计i的个数
	for i:=1;i<=100;i++{
		if i % 3 == 0 && i % 5 != 0{
			fmt.Print(i,"\t")
			count++
			if count % 5 == 0{
				fmt.Println()
			}
		}
	}
	fmt.Println()
	fmt.Println("共计数量：",count)


	//求1-5的和
	/*
	sum=0
	sum=+1+2+3+4+5

	sum=+i
	 */
	 sum := 0
	 for i:=1;i<=5;i++{
	 	sum += i//sum = sum + i
	 }
	 fmt.Println(sum)
	 /*
	 sum=0
	 i=1
	 i<=5-->sum=sum+i
	 	sum=+1
	 i++-->i=2
	 i<=5-->sum=sum+i
	 	sum = 1+2 = 3
	 i++-->i=3
	 i<=5-->sum=sum+i
	 	sum= 3+3 = 6
	 i++-->i=4
	 i<=5-->sum=sum+i
	 	sum =6+4=10
	 i++-->i=5
	 i<=5-->sum=sum+i
	 	sum = 10+5 = 15
	 i++-->i=6
	 i<=5-->false
	  */
}
