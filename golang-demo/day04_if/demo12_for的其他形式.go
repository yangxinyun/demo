package main

import "fmt"

func main()  {
	/*
	标准写法：
	for 表达式1;表达式2;表达式3{
		循环体
	}

	//同时省略3个表达式
	for{ //相当于for true，永真
	}
	//表达式1,2,3都可以省略不写
	 */
	 i:=1
	 for{
	 	fmt.Println(i)
	 	i++
	 }
	 for i:=1;;i++{
	 	fmt.Println(i)
	 	i++
	 }
}
