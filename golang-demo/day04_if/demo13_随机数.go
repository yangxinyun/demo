package main

import (
	"math/rand"
	"fmt"
	"time"
)

func main()  {
	/*
	随机数：伪随机数，随机数都算根据底层算法公式，算出来的。
		公式：需要一个种子数seed，



	random,随机数



	 */
	 //默认种子数为1。
	//获取系统当前的时间：time
	 t1:=time.Now()
	 fmt.Println(t1) //time
	 fmt.Printf("%T\n",t1) //time.Time
	 //时间戳：指定时间，距离1970年1月1日0点0时0分0妙,之间的时间差值：秒，纳秒
	 timeStamp1:=t1.Unix()//秒
	 fmt.Println(timeStamp1)
	timeStamp2:=t1.UnixNano()//纳秒
	fmt.Println(timeStamp2)
	 rand.Seed(timeStamp2)// 设置获取随机数的种子数：int64的数值即可。

	num1:= rand.Intn(10)//获取一个随机数：[0,n)
	fmt.Println(num1)
	num2 := rand.Intn(10)
	fmt.Println(num2)
	num3:=rand.Intn(10)
	fmt.Println(num3)

	//step1: 设置种子数
	rand.Seed(time.Now().UnixNano())
	//step2：获取随机数
	num4 := rand.Intn(100) //[0,100)
	fmt.Println(num4)
	/*
	[1,10]
	[0,9]+1

	[1,100]
	[0,99]+1

	[3,48]
	rand.Intn(46)+3


	[15,76]
	[0,61]+15
	rand.Intn(76-15+1)+15

	[m,n]
	rand.Intn(n-m+1)+m
	 */
	num5:= rand.Intn(10)+1//[0,9]+1-->[1,10]
	fmt.Println(num5)

	num6:=rand.Float64() //[0,1)
	fmt.Println(num6)
}
/*
猜数游戏：
1.系统产生一个个随机数：[1,100]
2.键盘输入：
	49
		提示：小了，下次大点
	78
		提示：大了，下次小点
	66
		提示：大了，下次小点
	55
		猜对了。。。
 */
