package main

import "fmt"

func main(){
	//定义一个四位数的整数，分别获取各个位数的值
	var num int
	fmt.Println("请输入一个4位的整数：")
	fmt.Scanln(&num)
 /*
 num :7954
 num / 1000
 num % 10

 7954-->954
 	7954 % 1000  /100
 7954-->79
 	7954 / 100 % 10

 7954 -->54 -->5

 7954 --> 795 -->5

  */
	qian := num % 10000 / 1000
	bai := num % 1000 / 100
	//bai2 := num / 100 % 10
	shi := num % 100 / 10
	//shi2 := num / 10 % 10
	ge := num % 10 / 1
	fmt.Printf("千位：%d,百位：%d,十位：%d,各位：%d\n",qian,bai,shi,ge)
	//fmt.Println(bai2,shi2)
}
