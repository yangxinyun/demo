package main

import (
	"fmt"
	"math/rand"
	"time"
)

func checkStudent(willSelectNum int) {

	students := []string{
		"冯颖聪", "徐鹏", "卢明建", "田中天",
		"陈紫娟", "蒲寒辉", "赵宇明", "舒训",
		"刘晓波", "王健", "孔兴亮", "田炳生",
		"张晓坤", "曾祥明", "陆奎", "朱军", "罗成省",
		"贾海", "张蔼琨", "杨欣博", "陈晶",
		"钱佳能", "陈晓会"}
	//存放随机选中的座位号
	var selectedNums []int
	time.Now().UnixNano()
	out:for i:=0;i<willSelectNum;{
		//产生随机数,从1到len(students)
	//LOOP:
		//timeS := time.Now().UnixNano()
		//randNum := rand.Int63n(timeS)%int64(len(students)) + 1
		randNum := rand.Intn(int(len(students)))+1
		//遍历已产生的号码,如果有重复的,重新产生一个
		for j := 0; j < i; j++ {
			if selectedNums[j] == randNum {
				continue out
			}
		}

		//这段输入23会陷入死循环
		//for _, studentNum := range selectedNums {
		//	if studentNum == randNum {
		//		goto LOOP
		//	}
		//}

		//不重复就添加到 checkedNums 中,选满12个人,停止
		if len(selectedNums) < willSelectNum {
			selectedNums = append(selectedNums, randNum)
		} else {
			fmt.Printf("\n")
			break
		}
		fmt.Printf("%d->%s\t\t", randNum, students[randNum-1])
		if len(selectedNums)%5 == 0 {
			fmt.Printf("\n")
		}
		i++
	}
}

func main() {
	fmt.Println(time.Now())
	var checkNum int
LOOP:
	fmt.Println("输入抽查人数1~23")
	fmt.Scanln(&checkNum)
	if checkNum > 23 || checkNum <= 0 {
		goto LOOP
	}
	checkStudent(checkNum)
}
