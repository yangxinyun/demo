package main

import (
	"fmt"
	"math"
)

func main() {
	/*
	 水仙花数：三位数：[100,999]
		每个位上的数字的立方和，刚好等于该数字本身。那么就叫水仙花数，4个
		比如：153
			1*1*1 + 5*5*5 + 3*3*3 = 1+125+27=153
	 */
	for i := 100; i < 1000; i++ {
		a := i / 100     //百位
		b := i / 10 % 10 //十位
		c := i % 10      //个位
		if math.Pow(float64(a), 3)+math.Pow(float64(b), 3)+math.Pow(float64(c), 3) == float64(i) {
			fmt.Println(i)
		}
	}

	fmt.Println("---------------------------------")
	/*
	a:1-9
	b:0-9
		10,11,12,13....19
		20,21,22.....29
		90.91,92,93..99
	c:0-9
	 */
	for a := 1; a < 10; a++ {
		for b := 0; b < 10; b++ {
			for c := 0; c < 10; c++ {
				n := a*100 + b*10 + c*1
				if a*a*a+b*b*b+c*c*c == n {
					fmt.Println(n)
				}
			}
		}
	}
}
