package main

import "fmt"

func main() {
	/*
	练习7：2 + 22 + 222+2222+22222
	 */
	var choose string
	for {//for true
		fmt.Println("请输入您的选择，n代表推出，其他任意键代表计算：")
		fmt.Scanln(&choose)
		if choose == "n" {
			fmt.Println("您选择推出。。")
			break //switch,for,用于强制推出for循环。
		} else {
			var num int
			var count int
			fmt.Println("请输入一个基数：(1-9)")
			fmt.Scanln(&num)
			fmt.Println("请输入累加的次数：")
			fmt.Scanln(&count)
			sum := 0
			temp := 0
			for i := 0; i < count; i++ {

				temp = temp*10 + num
				fmt.Print("+", temp)
				sum += temp //2,22,222,2222,22222
				/*
				2-->22
					2*10=20+2
				22-->222
					22*10=220+2
				222-->2222
					222*10=2220+2


				2=2*10^0
				22=2*10^1+2
				222=2*10^2+22
				 */
			}
			fmt.Println()
			fmt.Println(sum)
		}
	}

}
