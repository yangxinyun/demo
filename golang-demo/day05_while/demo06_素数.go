package main

import (
	"fmt"
	"math"
)

func main() {
	/*
	练习6：打印2-100内的素数

	只能被1和本身整除：从2开始，到前一个。。。

	7:
		2,3,4,5,6,
	8:
		2，3,4,5,6,7
	9:
		2,3
	10:
		2,
	11:
		2,3,4,5,6,7,8,9,10
	 */

	for i := 2; i <= 100; i++ {
		//count := 0 //用于统计i被整除的次数
		flag :=true//表示i是否三素数
		//验证
		for j := 2; j <= int(math.Sqrt(float64(i))); j++ {
			if i%j == 0 {
				//count++
				flag = false //不是素数
				break
			}
		}
		//结果
		//if count == 0 {
		//	fmt.Println(i, "是素数")
		//}
		if flag == true{
			fmt.Println(i)
		}
	}

}
