package main

import "fmt"

func main() {
	/*
	boolean sex = ...;

如果是true

就输出“是男人就。。。。。。”
	 */
	sex := false //true,男，false，女
	if sex { //== true
		fmt.Println("是男人就下100层。。。")
	}
	age := 20
	if !sex && age >= 18 { //sex == false ---> !sex
		fmt.Println("女大十八变。。")
	}

}
