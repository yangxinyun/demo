package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func main() {
	/*
	每个字母出现的次数
		map
			key,value
			key，每个字母
			value，字母出现的次数

	[E:3,C:1,A:2,L:1,I:1,]
	 */
	 str:="ECALIYHWEQAEFSZCVZTWEYXCPIURVCSWTDBCIOYXGTEGDTUMJHUMBJKHFGUKNKN"
	 map1 :=make(map[int]int)
	 fmt.Println(map1)
	 for i:=0;i<len(str);i++{
	 	val,ok := map1[int(str[i])]
	 	if ok{
	 		//str[i]，在map中已经有了，取value累加1
	 		val++
		}else{
			//str[i],在map中没有，val设置1
			val = 1
		}
		map1[int(str[i])] = val
	 }

	 //有序遍历
	 keys :=make([]int,0,len(map1))
	 for k:=range map1{
	 	keys = append(keys,k)
	 }
	 sort.Ints(keys)
	 for _,k:=range keys{
	 	fmt.Printf("%c,%d\n",k,map1[k])
	 }

	 fmt.Println("---------------------------")
	 //方法二
	 //str:="ECALIYHWEQAEFSZCVZTWEYXCPIURVCSWTDBCIOYXGTEGDTUMJHUMBJKHFGUKNKN"
	 for i:='A';i<='Z';i++{
	 	count:=0
	 	for j:=0;j<len(str);j++{
	 		if int32(str[j]) == i{
	 			count++
			}
		}
		fmt.Printf("%c,%d\n",i,count)
	 }

	 //方法三
	 for i:='A';i<='Z';i++{
	 	fmt.Printf("%c,%d\n",i,strings.Count(str,string(i)))
	 }

	 //基本数据类型和字符串之间的转换
	 /*
	 int :65---->string:"65"

	 string:"97"--->int:97
	  */
	 a:=65 //int
	 s1 := strconv.Itoa(a)
	 fmt.Println(s1)

	 s2 := "97abc"
	 b,err := strconv.Atoi(s2)
	 fmt.Println(b)
	 fmt.Println(err)


	 var c int64 = 20
	 s3:=strconv.FormatInt(c,8) //"24"
	 fmt.Println(s3)

	 d,_:=strconv.ParseInt(s3,8,64)
	 fmt.Println(d)
	 /*
	 ss1:=[]byte{65}
	 string(ss1)-->"A"
	  */


    fmt.Println("------------------------------")
	str="ECALIYHWEQAEFSZCVZTWEYXCPIURVCSWTDBCIOYXGTEGDTUMJHUMBJKHFGUKNKN"
    ss1:=strings.Split(str,"") //[]string
    fmt.Println(ss1)
    sort.Strings(ss1)
    fmt.Println(ss1)

    fmt.Println("----------------")
    //数组：index,value
    /*
    A：65
    Z:90
    a:97
    b:122

     */
     arr:=[91]int{}
     fmt.Println(arr)
     for i:=0;i<len(str);i++{
     	arr[str[i]]++
	 }
	 fmt.Println(arr)
	 for i,v:=range arr{
	 	if v !=0{
	 		fmt.Printf("%c,%d\n",i,v)

		}
	 }
}
