package main

import (
	"strings"
	"fmt"
)

func main() {
	/*
	将字符串"aa:zhangsan@163.com!bb:lisi@sina.com!cc:wangwu@126.com"，按照指定的key-value规则存入map中
	key：aa,bb,cc
	value:zhangsan@163.com,lisi@sina.com,wangwu@126.com
	 */
	 str:="aa:zhangsan@163.com!bb:lisi@sina.com!cc:wangwu@126.com"
	 map1:=make(map[string]string)

	 ss1:=strings.Split(str,"!")
	 for _,v:=range ss1{
	 	//fmt.Println(v)
	 	ss2:=strings.Split(v,":")
	 	map1[ss2[0]]=ss2[1]
	 }
	 fmt.Println(map1)
}
