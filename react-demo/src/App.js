import React from 'react';
import { Provider } from 'react-redux'
import store from './store'
import Todolist from './components/Todolist'
import Life from './components/Life'
import Animate from './components/Animate.jsx'
import Redux from './components/Redux'
import ReactRedux from './components/Reactredux'
import { HashRouter, Route } from 'react-router-dom'
import Header from './components/Header.jsx'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <Provider store={store}>

      <HashRouter>
        <Header></Header>
        <Route exact path="/todolist" component={Todolist} />
        <Route exact path="/life" component={Life} />
        <Route exact path="/animate" component={Animate} />
        <Route exact path="/redux" component={Redux} />
        <Route exact path="/reactredux" component={ReactRedux} />
      </HashRouter>
    </Provider>
  );
}

export default App;
