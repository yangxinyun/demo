import React, { Fragment } from 'react'
import '../App.css'
class Animate extends React.Component {
    constructor() {
        super()
        this.state = {
            show: true
        }
    }
    render() {
        return (<Fragment>
            <div className={this.state.show ? 'showa' : 'hidea'}>hello world</div>
            <button className="btn btn-default" onClick={this.toggle}>显示/隐藏</button>
            <hr /></Fragment>)
    }
    toggle = () => {
        this.setState({
            show: !this.state.show
        })
    }
}

export default Animate

