import React,{ Fragment } from 'react'
import { Link } from 'react-router-dom'

export default function Header(){
    return (
        <Fragment>
            <Link to="/todolist" className="btn btn-info">TodList</Link>
            <Link to="/life" className="btn btn-info">生命周期</Link>
            <Link to="/animate" className="btn btn-info">动画</Link>
            <Link to="/redux" className="btn btn-info">Redux</Link>
            <Link to="/reactredux" className="btn btn-info">React-redux</Link>
            <hr />
        </Fragment>
    )
}