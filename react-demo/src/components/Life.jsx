import React from 'react'


class Life extends React.Component {
    //组件初始化的时候执行
    constructor() {
        super()
        console.log("constructor：在组件初始化的时候执行")
    }

    change(e) {
        let newValue = e.target.value;
        this.setState({
            v: newValue
        })
    }

    render() {
        console.log("render：渲染DOM")
        return (<div>
            <ul>
                <li>constructor：在组件初始化的时候执行</li>
                <li>componentWillMount：在组件即将被挂载到页面的之前执行</li>
                <li>componentDidMount：组件被挂载到页面之后自动执行</li>
                <li>componentWillReceiveProps:</li>
                <li>shouldComponentUpdate：组件被更新之前会自动执行</li>
                <li>shouldComponentUpdate：组件被更新之前会自动执行</li>
                <li>componentWillUpdate:组件被更新之前执行，在shouldComponentWillUpdate之后执行</li>
                <li>componentDidUpdate:组件更新完成之后执行</li>
                <li>child componentWillUnmount:组件被销毁的时候执行</li>
            </ul>
            <hr />
        </div>)
    }

    //在组件即将被挂载到页面的之前执行的函数,还没有挂载到页面
    componentWillMount() {
        console.log("componentWillMount：在组件即将被挂载到页面的之前执行")
    }

    //组件被挂载到页面之后自动执行
    componentDidMount() {
        console.log("componentDidMount：组件被挂载到页面之后自动执行")
    }

    //一个组件要从父组件接受参数
    //如果这个组件第一次存在于父组件中，不会执行
    //如果这个组件之前已经存在于父组件中，才会执行
    componentWillReceiveProps() {
        console.log("componentWillReceiveProps:")
    }

    //组件被更新之前会自动执行,组件需要更新吗？
    shouldComponentUpdate() {
        console.log("shouldComponentUpdate：组件被更新之前会自动执行")
        return true;
    }


    //组件被更新之前执行，但是他在shouldComponentUpdate如果返回false则不会执行
    componentWillUpdate() {
        console.log("componentWillUpdate:组件被更新之前执行，在shouldComponentWillUpdate之后执行")
    }

    //组件更新完成之后执行
    componentDidUpdate() {
        console.log("componentDidUpdate:组件更新完成之后执行")
    }

    //当这个组件即将被从页面剔除的时候执行的函数
    componentWillUnmount() {
        console.log("child componentWillUnmount:组件被销毁的时候执行")
    }
}

export default Life