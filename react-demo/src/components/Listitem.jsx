import React from 'react'
import PropTypes from 'prop-types'

class Listitem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    render() {
        return (<div><h1 onClick={this.handleClick.bind(this)}>TodoList</h1>
            <p>{this.props.content}</p></div>)
    }
    handleClick() {
        alert(this.props.index)
    }
}

//类型校验
Listitem.propTypes = {
    content: PropTypes.string.isRequired
}

Listitem.defaultProps = {
    content: 'hello world'
}

export default Listitem