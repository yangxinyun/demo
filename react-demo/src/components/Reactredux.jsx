import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import * as actions from '../store/actions.js'
import { bindActionCreators } from "redux"

class Reactredux extends React.Component {
    zengjianumber() {
        var number = Number(this.refs.inputNumber.value)
        this.props.actions.zengjianumber(number)
    }
    render() {
        return (
            <Fragment>
                <h3>{this.props.v}</h3>
                <button className="btn btn-info" onClick={this.props.actions.zengjia}>增加</button>
                &nbsp;
                <button className="btn btn-info" onClick={this.props.actions.jianshao}>减少</button>
                <br /><br />
                <p>
                    <input type="number" ref="inputNumber" /><button className="btn btn-info" onClick={this.zengjianumber.bind(this, 2)}>增加</button>
                </p>
            </Fragment>
        )
    }
}

export default connect(
    (state) => {
        return {
            v: state.v
        }
    }, (dispatch) => {
        return { actions: bindActionCreators(actions, dispatch) }
    }
)(Reactredux)