const defaultState = {
    inputValue: '',
    list: [1, 2],
    v: 0
}


export default (state = defaultState, action) => {
    if (action.type === 'change_input_value') {
        let newState = JSON.parse(JSON.stringify(state))
        newState.inputValue = action.value
        return newState
    }
    else if (action.type === 'add_todo_item') {
        let newState = JSON.parse(JSON.stringify(state))
        newState.list.push(newState.inputValue)
        newState.inputValue = ''
        return newState
    }
    else if (action.type === 'remove_item') {
        let newState = JSON.parse(JSON.stringify(state))
        newState.list.splice(action.index, 1)
        return newState
    }
    else if (action.type === 'ZENGJIA') {
        let newState = JSON.parse(JSON.stringify(state))
        newState.v++
        return newState
    }
    else if (action.type === 'JIANSHAO') {
        let newState = JSON.parse(JSON.stringify(state))
        newState.v--
        return newState
    } else if (action.type === "ZENGJIANUMBER") {
        let newState = JSON.parse(JSON.stringify(state))
        newState.v = newState.v + action.number
        return newState
    }
    return state
}